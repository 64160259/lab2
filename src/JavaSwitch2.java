import javax.swing.plaf.synth.SynthSplitPaneUI;

public class JavaSwitch2 {
    public static void main(String[] args) {
        int day = 4;
        switch (day) {
            case 6:
                System.out.println("To day is Saturday");
                break;
            case 7:
                System.out.println("Today is Sunday");
                break;
            default: 
                System.out.println("Looking forward to the Weekend");
        }
        // Outputs "Looking forward to the Weekend"
    }

}
